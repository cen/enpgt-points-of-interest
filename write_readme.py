from typing import TextIO

from config import interested_in


def write_readme(f: TextIO, radii: list[int]):
    f.write(
        "OSM POINTS OF INTEREST TOOL\n\n"
        "This file contains some information on how the data was generated and how to interpret it."
    )

    f.write(
        "\n\n-----\n\n"
        "OSM OVERPASS API\n\n"
        "This tool uses the Overpass API to extract OSM data.\n"
        "OSM is a public dataset for street networks, buildings, and other geographical or infrastructural"
        " features.\n"
        "The Overpass API allows you to collect all roads, buildings etc. within a specified area.\n"
        "In this case, we are interested in features that are marked with"
        ' specific tags such as "tourism", "shop" and "amenity", which we call Points of Interest.'
    )

    f.write(
        "\n\n-----\n\n"
        "INPUT AND OUTPUT\n\n"
        "Each line in the task file defines a location.\n"
        "The OSM Points of Interest Tool will count features (buildings) of different types within a"
        " a radius around each location.\n\n"
    )

    radius_text = f"In this case, the radius {radii[0]}m was used."
    if len(radii) > 1:
        radius_text = (
            f"In this case, the radii {', '.join(str(radius) + 'm' for radius in radii[:-1])}"
            f" and {radii[-1]}m were used.\n"
            f"For each radius, you'll be able to find several output files"
            f' whose names start with the radius, such as "output_{radii[0]}_total.csv".'
        )

    f.write(radius_text + "\n")

    f.write(
        "Furthermore, the OSM Points of Interest Tool, beyond just a total count of POIs, will extract"
        " individual counts for specific types and subtypes.\n"
    )
    f.write("As such, there will be multiple output files" + ".\n" if len(radii) == 1 else "per radius.\n")
    f.write(
        f'For example, the file "output_{radii[0]}_amenity.csv" will have counts for individual types of'
        ' amenities, such as "fuel", "cinema" or "restaurant".'
    )

    f.write("\n\n-----\n\nWHAT EXACTLY WAS DONE FOR THIS TASK?\n\n")

    f.write(
        "For each location, a query to the OSM Overpass API is made for features in a radius around the"
        " location's coordinate.\n"
        "This query is restricted to Points of Interest, which are defined as features with "
    )
    if len(interested_in) > 1:
        f.write(f" any of the following tags: {', '.join(interested_in)}.\n")
    else:
        f.write(f"the {interested_in[0]} tag.\n")

    f.write("These features were then further classified and sub-classified by tags and subtags.\n")

    f.write("\n-----\n\nOUTPUT\n\n")

    f.write(
        'Output files ending in "total" will have the total count of features that have any of these tags.\n'
        "Individual counts per tag will be included as additional columns.\n\n"
    )
    f.write(
        'In addition to the "total" file, there will also be one more file per tag.\n'
        "This is because tags have subtypes.\n"
        "In the file for a tag, you will find the total count of features "
        'with that tag (which should match the count in that tag\'s column "total" file),\nbut you will also '
        "find one column per subtype that was found.\n\n"
        "Lastly, while it is rare, it is possible for a feature to have multiple tags.\n"
        "If this is the case, there will be a third type of file for tag combinations.\n"
        "A feature with multiple tags will count towards the count in the combination file as well as each"
        " of its tags' individual files."
    )
