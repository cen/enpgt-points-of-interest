import os.path
from dataclasses import dataclass, field
from pathlib import Path
from typing import Optional

from BaseClasses import ToolDefinition, ToolDefinitionArgs
from tools.OSMnxPointsOfInterestTool.main import main


class OSMnxPointsOfInterestToolClass(ToolDefinition):
    tool_name: str = "OSMnxPointsOfInterestTool"
    metric_name: str = "OSMnx Points of Interest"
    supports_multipoly_mode = True

    @dataclass
    class OSMnxPointsOfInterestToolArgs(ToolDefinitionArgs):
        radii: list[int] = field(default_factory=lambda: [100, 200, 500, 1000, 2000])
        osmnx_endpoint: Optional[str] = None

    @classmethod
    def supports_coordinate(cls, lat: float, lng: float) -> bool:
        return True

    @classmethod
    def required_setup(cls, _: OSMnxPointsOfInterestToolArgs):
        return

    @classmethod
    def main(cls, args: OSMnxPointsOfInterestToolArgs) -> Path:
        sysargs = [str(args.radii).replace(" ", ""), os.path.realpath(args.task_file_location)]

        if args.osmnx_endpoint is not None:
            sysargs.append("osm_url=" + args.osmnx_endpoint)

        correct_dir = os.getcwd()

        tool_dir = os.path.dirname(os.path.realpath(__file__))

        output_directories_before = []
        if os.path.isdir(os.path.join(tool_dir, "outputs")):
            output_directories_before = [
                Path(tool_dir, "outputs", p) for p in os.listdir(os.path.join(tool_dir, "outputs"))
            ]

        os.chdir(tool_dir)

        main(sysargs)

        os.chdir(correct_dir)

        output_directories_after = []
        if os.path.isdir(os.path.join(tool_dir, "outputs")):
            output_directories_after = [
                Path(tool_dir, "outputs", p) for p in os.listdir(os.path.join(tool_dir, "outputs"))
            ]

        new_output_dirs = list(set(output_directories_after) - set(output_directories_before))
        assert len(new_output_dirs) == 1

        # TODO: Write readme

        return new_output_dirs[0]
