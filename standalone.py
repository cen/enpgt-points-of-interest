from datetime import datetime
from pathlib import Path

from main import do_pois
from write_readme import write_readme

radii: list[int] = [100, 200, 500, 1000, 2000]
osm_endpoint = None  # None for default

task_file = Path("task.txt")
output_dir = Path("outputs", datetime.now().strftime("%d-%m-%Y_%H-%M-%S"))

if __name__ == "__main__":
    for radius in radii:
        radius_directory = Path(output_dir, f"{radius}m")
        do_pois(
            task_file,
            radius_directory,
            radius,
            osm_url=osm_endpoint,
        )

    with open(Path(output_dir, "readme.txt"), "w") as readme_file:
        write_readme(readme_file, radii)
