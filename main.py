import ast
import logging
import os
import shutil
import sys
from collections import Counter
from datetime import datetime
from itertools import combinations
from multiprocessing import Pool
from pathlib import Path
from typing import Optional

import osmnx
import osmnx._errors
import osmnx.settings
import pyproj
import shapely.ops
from config import interested_in
from enpgt_task_file_reader import convert_nsv_task_file_to_polygons
from geopandas import GeoDataFrame
from shapely import Polygon, geometry
from tqdm import tqdm
from write_readme import write_readme


def get_frame_per_poi_type(
    poly_in_wgs: shapely.Polygon | shapely.MultiPolygon, datetime_str: Optional[str]
) -> dict[tuple[str, ...], GeoDataFrame]:
    if datetime_str:
        osmnx.settings.overpass_settings = '[out:json][timeout:{timeout}][date:"' + datetime_str + '"]{maxsize}'
    else:
        osmnx.settings.overpass_settings = "[out:json][timeout:{timeout}]{maxsize}"

    try:
        a = osmnx.features_from_polygon(poly_in_wgs, tags={key: True for key in interested_in})
    except osmnx._errors.InsufficientResponseError:
        a = GeoDataFrame(columns=interested_in)

    individuals = {}

    for i in range(0, len(interested_in) + 1):
        for comb in combinations(interested_in, i):
            corresponding_dataframe = a

            for key in comb:
                if key in corresponding_dataframe.columns:
                    corresponding_dataframe = corresponding_dataframe.dropna(subset=key)
                else:
                    corresponding_dataframe = GeoDataFrame(columns=interested_in)

            individuals[comb] = corresponding_dataframe

    return individuals


def do_single_poi(
    poly_in_wgs: shapely.Polygon | shapely.MultiPolygon, datetime_str: Optional[str]
) -> dict[str, dict[str, int]]:
    dataframes_per_type = get_frame_per_poi_type(poly_in_wgs, datetime_str)

    results_total = {}

    for combination, dataframe in dataframes_per_type.items():
        if not combination:
            continue

        if not len(dataframe):
            results_total[", ".join(combination)] = {"total": 0}
            continue

        subtypes = Counter()
        subtypes["total"] = len(dataframe)

        for feature in dataframe.iterfeatures():
            subtype_values = []
            for key in combination:
                subtype_values.append(f"{key}:{feature['properties'][key]}")
            subtypes[", ".join(subtype_values)] += 1

        results_total[", ".join(combination)] = subtypes

    results_total_real = {
        "total": {"total": len(dataframes_per_type[()])} | {k: v["total"] for k, v in results_total.items()}
    }

    results_total_real.update(results_total)

    return results_total_real


def do_single_poi_star(args: tuple[shapely.Polygon | shapely.MultiPolygon, Optional[str]]) -> dict[str, dict[str, int]]:
    return do_single_poi(*args)


def get_poly_from_radius(lat: float, lng: float, radius: int) -> shapely.Polygon | shapely.MultiPolygon:
    crs_wgs84 = pyproj.Proj("WGS84")
    crs_aeqd = pyproj.Proj(proj="aeqd", datum="WGS84", lon_0=lng, lat_0=lat, units="m")
    reverse_transproj = pyproj.transformer.Transformer.from_proj(crs_aeqd, crs_wgs84, always_xy=True)

    radius_poly = geometry.Point(0, 0).buffer(radius, resolution=1)
    radius_poly_wgs: Polygon = shapely.ops.transform(reverse_transproj.transform, radius_poly)

    return radius_poly_wgs


def initializer(osm_url: Optional[str]):
    if osm_url is None:
        logging.info("Default OSM endpoint used.")
        return

    osmnx.settings.overpass_endpoint = osm_url
    osmnx.settings.overpass_rate_limit = False
    osmnx.settings.use_cache = False
    logging.info(f"Using OSM endpoint {osm_url}")


def do_pois(
    infile: Path, output_dir: Path, radius: int, datetime_str: Optional[str] = None, osm_url: Optional[str] = None
):
    print("---")
    print(f"Getting Points of Interest stats for file {infile} with radius {radius}m.")

    if not output_dir.exists():
        output_dir.mkdir(parents=True, exist_ok=True)

    shutil.copyfile(infile, os.path.join(output_dir, os.path.basename(infile)))

    geo_id_to_polygon = convert_nsv_task_file_to_polygons(infile, radius)

    if not geo_id_to_polygon:
        return

    starmap = [(poly, datetime_str) for poly in geo_id_to_polygon.values()]

    with Pool(initializer=initializer, initargs=(osm_url,)) as pool:
        outputs = list(tqdm(pool.imap(do_single_poi_star, starmap), total=len(geo_id_to_polygon), desc="Doing POI"))

    first_elem = next(iter(outputs))

    for comb in first_elem:
        all_subtypes = {}
        for output in outputs:
            all_subtypes.update(output[comb])

        if len(all_subtypes) == 1:
            continue

        with open(os.path.join(output_dir, f"output_{radius}_{comb.replace(', ', '-')}.csv"), "w") as outfile:
            outfile.write("geo_id," + ",".join(tok.replace(", ", "&") for tok in all_subtypes) + "\n")

            for index, geo_id in enumerate(geo_id_to_polygon):
                values_string = ",".join(
                    str(outputs[index][comb][typ]) if typ in outputs[index][comb] else "0" for typ in all_subtypes
                )
                outfile.write(geo_id + "," + values_string + "\n")


def main(args):
    default_task_file = Path("task.txt")

    radii = [100, 200, 500, 1000, 2000]

    datetime_str: Optional[str] = None
    year = None

    osm_url = None

    for arg in args:
        if arg.startswith("[") and arg.endswith("]"):
            radii = ast.literal_eval(arg)

        elif arg.startswith("osm_url="):
            osm_url = arg[8:]
            if not osm_url.endswith("api"):
                osm_url = osm_url.rstrip("/") + "/api"

        elif arg.startswith("year="):
            year = arg[5:]

        elif arg.isnumeric():
            radii = [int(arg)]

        elif os.path.isfile(arg):
            default_task_file = Path(arg)

        else:
            print(f"Could not find file {arg}.")
            return

    if not default_task_file.is_file():
        print(f"No task file was specified, and {default_task_file} was not found. Don't know what to work on.")
        return

    output_dir = Path("outputs", datetime.now().strftime("%d-%m-%Y_%H-%M-%S"))

    if year:
        datetime_str = f"{year}-07-01T00:00:00Z"

    for radius in radii:
        radius_directory = Path(output_dir, f"{radius}m")
        do_pois(default_task_file, radius_directory, radius, datetime_str, osm_url)

    with open(Path(output_dir, "readme.txt"), "w") as readme_file:
        write_readme(readme_file, radii)


if __name__ == "__main__":
    main(sys.argv[1:])
